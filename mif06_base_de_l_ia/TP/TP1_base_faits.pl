/* les hommes */ 
homme(alphonse). 
homme(bernard). 
homme(cedric). 
homme(david). 
homme(fabien). 
homme(gerard). 
homme(hubert). 
homme(jules). 
homme(kevin). 
homme(lucien).  

/* les femmes */ 
femme(charlotte).
femme(daniela). 
femme(eloise).   
femme(marie). 
femme(nathalie). 
femme(ophelie). 
femme(stephanie). 
femme(violette). 
femme(zoe). 

/* les relations de parentÃ© */
/* oÃ¹ enfant(X,Y) signifie que X est enfant de Y */ 
enfant(alphonse,bernard). 
enfant(alphonse,eloise). 
enfant(cedric,fabien). 
enfant(cedric,nathalie). 
enfant(daniela,kevin). 
enfant(daniela,charlotte). 
enfant(fabien,lucien). 
enfant(fabien,marie). 
enfant(gerard,lucien). 
enfant(gerard,marie). 
enfant(nathalie,david). 
enfant(nathalie,zoe). 
enfant(ophelie,hubert). 
enfant(ophelie,marie). 
enfant(stephanie,alphonse). 
enfant(stephanie,violette). 
enfant(violette,zoe). 
enfant(violette,jules). 
enfant(charlotte,zoe). 
enfant(charlotte,jules). 