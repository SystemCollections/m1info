/* Name: simul_compare_classique.c
 *
 * Authors: C. Crespelle
 * Mail   : Christophe.Crespelle@inria.fr 
 * Purpose: Heuristic for cograph edition and completion of complex networks
 ***************************************************************************/

#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
//#include <unistd.h>

#include "auxiliaire.c"



/******************/
/* Default values */
/******************/




/********************/
/* Output functions */
/********************/

// print help
void usage(char *c){
  fprintf(stderr,"Usage: %s -h\n",c);
  fprintf(stderr,"  -h: print current help.\n");
  fprintf(stderr,"  -i \"inputname\": input file containing the graph (default value is stdin).\n");
  fprintf(stderr,"  -o \"outputname\": output results in a file named \"outputname\" (default value is stdout).\n");
  exit(-1);
}




/**************************************************************/
/**************************************************************/
/**************************************************************/
/***************************        ***************************/
/***************************  MAIN  ***************************/
/***************************        ***************************/
/**************************************************************/
/**************************************************************/
/**************************************************************/

int main(int argc, char **argv){


///////////////////////////////////////////////////////////
//////////   DECLARATIONS AND DEFAULT VALUES   ////////////
///////////////////////////////////////////////////////////
 
 	FILE* fin=NULL;
  FILE* fout=NULL;

	int i;







///////////////////////////////////////////////////////////
////////////////   PARSE COMMAND LINE   ///////////////////
///////////////////////////////////////////////////////////


  // default values
  char name_in[100]="";
  char name_out[100]="";
  int input_file_given=0;
  int output_file_given=0;

  // user's values
  
  for (i=1; i<argc; i++) {
    if ((strcmp(argv[i],"-h")==0) || (strcmp(argv[i],"--help")==0) ) {
      usage(argv[0]);     
    }
    else if ((strcmp(argv[i],"-i")==0) || (strcmp(argv[i],"--input")==0) ) {
      if (i==argc-1)
	      usage(argv[0]);
      input_file_given=1;
      strcpy(name_in,argv[++i]);
    }
    else if ((strcmp(argv[i],"-o")==0) || (strcmp(argv[i],"--output")==0) ) {
      if (i==argc-1)
	      usage(argv[0]);
	    output_file_given=1;
      strcpy(name_out,argv[++i]);
    }
    else usage(argv[0]);
  }
    
  
  
  
  
///////////////////////////////////////////////////////////
///////////////////   OPEN FILES   ////////////////////////
///////////////////////////////////////////////////////////



	if (input_file_given==0) {
  	fin=stdin;
  }
	else {
	  if ( (fin=fopen(name_in,"r"))==NULL)
	    report_error("name_in -- fopen: error");
	}


	if (output_file_given==0) 
		fout=stdout;
	else {
			if ( (fout=fopen(name_out,"w"))==NULL)
		    report_error("name_out -- fopen: error");
	}
	
	

///////////////////////////////////////////////////////////
/////////   PRINT MAX VALUES ON THE MACHINE   /////////////
///////////////////////////////////////////////////////////



  	


///////////////////////////////////////////////////////////
/////////////////   LOADING GRAPH   ///////////////////////
///////////////////////////////////////////////////////////






///////////////////////////////////////////////////////////
//////////////////   DATA STRUCTURE   /////////////////////
///////////////////////////////////////////////////////////

  
  
  
  

///////////////////////////////////////////////////////////
/////////////////   COMPUTE MIN CUT   /////////////////////
///////////////////////////////////////////////////////////

	fprintf(stderr,"Begin computation.\n");
  fflush(stderr);





///////////////////////////////////////////////////////////
//////////////////   OUTPUT RESULTS   /////////////////////
///////////////////////////////////////////////////////////


  fprintf(stderr,"Outputing results.\n");
  fflush(stderr);





///////////////////////////////////////////////////////////
////////////   CLOSE FILES AND FREE MEMORY  ///////////////
///////////////////////////////////////////////////////////



	if (fin!=NULL) fclose(fin);
	fprintf(stderr,"Input file closed.\n");
  fflush(stderr);
	
	if (fout!=NULL) fclose(fout);
	fprintf(stderr,"Output file closed.\n");
  fflush(stderr);




  return 0;
}

/**************************************************************/
/**************************************************************/
/*************************            *************************/
/*************************  FIN MAIN  *************************/
/*************************            *************************/
/**************************************************************/
/**************************************************************/


